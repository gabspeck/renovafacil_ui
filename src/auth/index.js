import Vue from 'vue'
import router from '../router'

var token = ''

export function login (context, creds, redirect) {
  context.$http.post('/api/login', creds).then(data => {
    if (redirect) {
      context.$router.push(redirect)
    }
  }, e => {
    e.json().then(j => { context.error = j.message })
  })
}

export function getToken () {
  if (token !== '') {
    return token
  }
  return Vue.http.get('/api/token').then(r => {
    token = r.headers.get('X-CSRF-Token')
    return token
  })
}

export function isLoggedIn () {
  return !!Vue.cookie.get('SESSION')
}

export function logout () {
  token = ''
  Vue.cookie.delete('SESSION')
  Vue.http.get('/api/logout').then(router.push('/login'))
}

