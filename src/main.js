// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App'
import router from './router'
import http from 'vue-resource'
import cookie from 'vue-cookie'
import {getToken, logout} from './auth'

Vue.config.productionTip = true
Vue.use(ElementUI)
Vue.use(http)
Vue.use(cookie)

/* eslint-disable no-new */
new Vue({ el: '#app',
  router,
  components: { App },
  render: h => h(App)
})

Vue.http.interceptors.push(async (request, next) => {
  if (['POST', 'PUT', 'DELETE'].includes(request.method)) {
    let token = await getToken()
    request.headers.set('X-CSRF-Token', token)
  }
  next()
})

Vue.http.interceptors.push((request, next) => {
  next(r => {
    if (r.status === 401) {
      logout()
    }
  })
})

