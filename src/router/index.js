import Vue from 'vue'
import Router from 'vue-router'

import Accounts from '@/components/Accounts'
import Login from '@/components/Login'
import RenewalHistory from '@/components/RenewalHistory'
import Renewals from '@/components/Renewals'

import {isLoggedIn} from '../auth'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/renovacoes'
    },
    {
      path: '/renovacoes',
      component: Renewals,
      meta: {
        title: 'Renovações'
      }
    }, {
      path: '/historico',
      component: RenewalHistory,
      meta: {
        title: 'Histórico'
      }
    },
    {
      path: '/contas',
      component: Accounts,
      meta: {
        title: 'Contas'
      }
    },
    {
      path: '/login',
      component: Login,
      meta: {
        title: 'Login'
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  document.title = 'RenovaFácil'
  if (!to.path.startsWith('/login') && !isLoggedIn()) {
    next('/login')
    return
  }
  if (to.meta.title) {
    document.title = to.meta.title + ' | ' + document.title
  }
  next()
})

export default router
